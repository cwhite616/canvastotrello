// Minify the code at https://minify.js.org/
// Convert it to a bookmarklet at https://mrcoles.com/bookmarklet/

alert("Click assignments to copy them to Trello.")

function getConfig() {
    return {
        "Trello Token" : "", // trello developer token
        "Trello key" : "", // trello key
        "Board ID" : "", // trello board id, not needed actually
        "List ID" : "", // trello target list id
    }
}

function getCanvasClassName() {
    var className = document.getElementById("mobile-header").getElementsByClassName("mobile-header-title")[0].getElementsByTagName("span")[0].innerHTML
    if (className.length > 0) {
        return className
    }
    return false;
}

function getCanvasAssignments(as) {
  var oa = {
    "class" : getCanvasClassName(),
    "id" : as["id"],
    "title" : as.getElementsByClassName("ig-title")[0].innerHTML.trim(),
    "link" :  as.getElementsByClassName("ig-title")[0].href,
    "due" : as.getElementsByClassName("ig-details__item assignment-date-due")[0].getElementsByClassName("screenreader-only")[0].innerHTML
  }
  return oa
}

function mapClassToLabel(className) {
    if(className == "6th Grade English-Marlow") {
        return "5f5826e9cdabcf46c06fa56e"
    }
    if(className == "6th Grade Math ABC-Packard") {
        return "5f5826e9cdabcf46c06fa571"
    }
    if(className == "6th Grade Physical Education-Dietz") {
        return "5f5a06535884475734dd4a37"
    }
    if(className == "6th Grade Science-Lantz") {
        return "5f5826e9cdabcf46c06fa572"
    }                  
     if(className == "6th Grade Social Studies-Rood") {
        return "5f5826e9cdabcf46c06fa574"
    }  
    if(className == "6th Grade Spanish-Buxton") {
        return "5f5826e9cdabcf46c06fa577"
    }  
    if(className == "6th Grade Viola-Mitchell") {
        return "5f582a3355320b262bc35b47"
    }
    if(className == "6th Grade Art-Lampen") {
        return "5f5826e9cdabcf46c06fa56b"
    }     
    return;
}

document.addEventListener('click', function(e) {
    e = e || window.event;
    var target = e.target || e.srcElement,
        text = target.textContent || target.innerText;
    var p = getParentOfClass(target, "ig-row")
    if (p) {
        var c = getCanvasAssignments(p)
        var d = new Date.parse(c.due)
        // createTrelloCard(name, description, pos, due, list, labels, idMembers)
        createTrelloCard(c.title, c.link, "top", d.toISOString(), getConfig()["List ID"], mapClassToLabel(c.class))
    }
}, false);

function getParentOfClass(el, matchClass) {
    if (el.classList && el.classList.contains(matchClass)) {
        return el;
    }
    if (el.parentNode == null) {
        return null;
    }
    return getParentOfClass(el.parentNode, matchClass)
}

function createTrelloCard(name, description, pos, due, list, labels, idMembers) {
    var payload = {
        "name":name, //(required) Valid Values: a string with a length from 1 to 16384
        "desc":description, //(optional)Valid Values: a string with a length from 0 to 16384
        "pos":pos, //(optional) Default: bottom Valid Values: A position. top, bottom, or a positive number.
        "due": due, //(required) Valid Values: A date, or null
        "idLabels": labels ,//(optional)
    };

    var trellourl = 'https://api.trello.com/1/cards?key='+getConfig()['Trello key']+'&token='+getConfig()['Trello Token']+'&idList='+getConfig()['List ID'];
    for (x in payload) {
        trellourl += "&" + x + "=" + encodeURIComponent(payload[x])
    }
    var trelloxhr = new XMLHttpRequest();
    trelloxhr.open('POST', trellourl, true);
    trelloxhr.onload = function(){
        console.log("Done");
        console.log(this.response);
        alert("Assignment added!")
    };
    trelloxhr.send(options)

    return false; 
}